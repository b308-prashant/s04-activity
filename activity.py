import pandas as pd
artist_profile = {
    "artist_name": "John Smith",
    "company": "XYZ Records",
    "age": 32
}
band_profile = {
    "band_name": "The Rockstars",
    "years_active": 10,
    "hit_songs": ["Song 1", "Song 2", "Song 3"],
    "is_active": True
}

merged_profile = {}
merged_profile.update(artist_profile)
merged_profile.update(band_profile)

print(merged_profile)

personal_info = {
    "name": "Jean",
    "age": 31,
    "salary": 45000,
    "city": "Seoul"
}

new_dict_a = dict(name=personal_info["name"], salary=personal_info["salary"])


new_dict_b = {}

for key, value in personal_info.items():
    if key == "name" or key == "salary":
        new_dict_b.update({key: value})

print(new_dict_a)
print(new_dict_b)

employees = {
    'emp1' : {"full name" : "Amy Santingo", "Salary" : 45000},
    'emp2' : {"full name" : "Charles Boyle", "Salary" : 50000},
    'emp3' : {"full name" : "Rosa Diaz", "Salary" : 40000},
    'emp4' : {"full name" : "Jake Peralta", "Salary" : 45000}
}

employees['emp4']['Salary'] = 55000



exam_data = {
    'name': ['Dwight', 'Michael', 'Jim', 'Pam', 'Andy'],
    'score': [12.5, 5, 10, 16.5, 9],
    'attempts': [1, 3, 2, 1, 3],
    'qualify': ['yes', 'no', 'yes', 'yes', 'no']
}

df = pd.DataFrame(exam_data)
print(df)

jim_row = df.loc[df['name'] == 'Jim']
print(jim_row)

